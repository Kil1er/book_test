require 'watir-webdriver'
require "watir-webdriver/extensions/alerts"


Before do
  @browser = Watir::Browser.new :firefox
  @browser.driver.manage.window.maximize
end


After do
  @browser.close
end