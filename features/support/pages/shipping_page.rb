class ShippingPage < BasePage
  include PageObject

  text_field :phone_number, id: 'account_phone'
  link :send_order, id: 'checkoutCart'
  radio_button :address, css: '.form input'


  def checkout
    wait_until { phone_number_element.visible? }
    address_element.click
    self.phone_number = CREDENTIALS[:phone]
    send_order_element.click
  end

end