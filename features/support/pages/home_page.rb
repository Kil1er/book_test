class HomePage < BasePage
  include PageObject

  page_url "http://retail.circlesoft.net/"

  link :log_in, css: '.login-link a'
  link :register, css: '.nav-link:nth-child(3) a'
  link :cart, css: '.cart-link a'

  def sign_up
    wait_until { register_element.visible? }
    register_element.click
  end

  def sign_in
    wait_until { register_element.visible? }
    log_in_element.click
  end

end