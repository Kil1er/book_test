require_relative '../pages/modules/success_message'

class ProductPage < BasePage
  include PageObject
  include SuccessMessage

  page_url "https://retail.circlesoft.net/catalog/21416-Test"

  links :add_to_cart, class: 'add-to-cart'
  link :go_to_cart, class: 'go-to-cart'

  def add_items(quantity)
    while quantity >= 0 do
      add_to_cart_elements.sample.click
      quantity -= 1
    end
  end

  def open_cart
    go_to_cart_element.click
  end
end