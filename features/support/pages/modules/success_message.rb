module SuccessMessage
  include PageObject

  p :success_message, id: 'flash_success'

  def have_success_message?
    success_message_element.visible?
  end
end