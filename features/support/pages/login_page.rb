class LoginPage < BasePage
  include PageObject

  text_field :user_name, id: 'user_name'
  text_field :password, id: 'password'
  button :log_in_button, xpath: ".//*[@id='inner-container']/div/div[2]/div[2]/fieldset/form[1]/div[3]/div/input[2]"

  def sign_in
    wait_until { user_name_element.visible? }
    self.user_name = CREDENTIALS[:username]
    self.password = CREDENTIALS[:password]
    log_in_button
  end
end