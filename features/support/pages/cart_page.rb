class CartPage < BasePage
  include PageObject

  text_fields :item_notes, css: '.item-notes input'
  links :remove, css: '.product-remove a'
  text_area :order_notes, id: 'client_note'
  radio_button :account, id: 'po_option_account'
  link :checkout, css: '.pull-right .checkoutCart.btn-large'
  select_list :shipping, id: 'shipping_select'
  span :total_cost, id: 'sub_total'
  span :quantity_total, id: 'quantity_total'

  def make_notes
    wait_until { checkout_element.visible? }
    note = self.item_notes_elements.sample
    note.send_keys("testing order notes")
    self.order_notes = "testing order notes"
  end

  def remove_items(quantity)
    quantity_of_elements = remove_elements.length
    p quantity_of_elements
    while quantity > 0 do
      remove_elements.sample.click
      @browser.driver.switch_to.alert.accept
      quantity -= 1
      wait_until{ quantity_total.to_i == quantity_of_elements - 1 }
      p quantity_total.to_i
    end

  end

  def choose_account_as_payment
    account_element.click
    checkout_element.click
  end

  def select_shipping
    shipping_element.select_value(1032)
    wait_until{ !ajax_active? }
  end

end