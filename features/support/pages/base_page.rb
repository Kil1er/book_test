class BasePage
  include PageObject

  link :go_to_cart, class: 'go-to-cart'

  CREDENTIALS ||= data_load[:credentials]

  def ajax_active?
    browser.execute_script("return $.active != 0")
  end
end