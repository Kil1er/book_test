class RegistrationPage < BasePage
  include PageObject

  text_field :first_name, id: 'user_first_name'
  text_field :last_name, id: 'user_last_name'
  text_field :email, id: 'account_email'
  text_field :password, id: 'account_password'
  text_field :confirm_password, id: 'account_password_confirmation'
  button :submit, class: 'create_button'

  def register
    wait_until { first_name_element.visible? }
    self.first_name = 'Akuma'
    self.last_name = 'Deus'
    self.email = "#{[*('A'..'Z')].sample(8).join}@mailinator.com"
    self.password = 'Qwertyu1'
    self.confirm_password = 'Qwertyu1'
    submit_element.click
  end

end