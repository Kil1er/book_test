require 'rspec'
require 'page-object'
require 'require_all'

require_relative 'data_load'

require_all File.dirname(__FILE__) + "/pages"

World(PageObject::PageFactory)
