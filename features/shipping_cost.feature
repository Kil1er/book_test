Feature: Changing of shipping cost
  In manage cart
  As a User
  I would like to see changing shipping cost in cart

Scenario: Changing shipping cost
  Given I am on home page
  And I sign in as register user
  When I add 3 items to cart
  And I select shipping
  And I remove 1 item from cart
  Then I should see changed shipping cost