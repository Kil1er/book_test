Feature: Making order
  In order to buy book
  As a User
  I would like to make order

Scenario: Making proper order
  Given I am on home page
  And I sign in as register user
  Then I should see successful message
  When I add 1 items to cart
  And I select shipping
  And I put a note
  And I select 'account' as payment type
  And I check out
  Then I should see posted order