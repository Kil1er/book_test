Feature: Sign up a new user
  In order to use functionality of site
  As a User
  I would like to register

Scenario: Register new user
  Given I am on home page
  When I register new user
  Then I should see successful message