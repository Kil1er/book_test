When /^I sign in as register user$/ do
  on(HomePage).sign_in
  on(LoginPage).sign_in
end

When /^I add (\d) items to cart$/ do | quantity |
  visit ProductPage
  on(ProductPage).add_items(quantity.to_i)
  on(ProductPage).open_cart
end

When /^I put a note$/ do
  on(CartPage).make_notes
end

When /^I select 'account' as payment type$/ do
  on(CartPage).choose_account_as_payment
end

When /^I select shipping$/ do
  on(CartPage).select_shipping
  cost = on(CartPage).total_cost_element.text
  cost[0] = ""
  $cost = cost.to_i
end

When /^I check out$/ do
  on(ShippingPage).checkout
end

When /^I remove (\d) item from cart$/ do | quantity |
  on(CartPage).remove_items(quantity.to_i)
end

Then /^I should see posted order$/ do
  expect(on(ProductPage).have_success_message?).to be true
end

Then /^I should see changed shipping cost$/ do
  current_cost = on(CartPage).total_cost_element.text
  current_cost[0] = ""
  p $cost
  p current_cost.to_i
  expect(current_cost.to_i < $cost ).to be true
end