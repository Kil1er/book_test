Given /^I am on home page$/ do
  visit HomePage
end

When /^I register new user$/ do
  on(HomePage).sign_up
  on(RegistrationPage).register
end

Then /^I should see successful message$/ do
  expect(on(ProductPage).have_success_message?).to be true
end
